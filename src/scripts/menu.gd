extends Node2D

onready var player_1_name_label = get_node("player_1_name_label")
onready var player_2_name_label = get_node("player_2_name_label")
onready var player_1_name_text_area = get_node("player_1_name")
onready var player_2_name_text_area = get_node("player_2_name")
onready var names_entered_button = get_node("names_entered_button")
onready var human_button = get_node("human_button")
onready var ai_button = get_node("ai_button")
onready var h_box = get_node("HBoxContainer")

var bots = {"dummy": "res://src/scripts/bots/dummy.gd"}

var board_path = "res://src/scenes/board.tscn"

func _on_human_button_pressed():
	make_buttons_invisible()
	player_1_name_label.visible = true
	player_2_name_label.visible = true
	player_1_name_text_area.visible = true
	player_2_name_text_area.visible = true
	names_entered_button.visible = true


func _on_names_entered_button_pressed():
	GameInfo.opponent_tyoe = "human"
	GameInfo.player_1_name = player_1_name_text_area.text
	GameInfo.player_2_name = player_2_name_text_area.text
	get_tree().change_scene(board_path)


func _on_ai_button_pressed():
	make_buttons_invisible()
	for i in range(bots.keys().size()):
		var bot_name = bots.keys()[0]
		var button = Button.new()
		button.text = bot_name
		h_box.add_child(button)
		button.connect("pressed", self, "_on_choose_ai_button_pressed", [bot_name])
	h_box.visible = true


func _on_choose_ai_button_pressed(bot_name):
	GameInfo.bot_script = bots[bot_name]
	GameInfo.player_1_name = "you"
	get_tree().change_scene(board_path)


func make_buttons_invisible():
	human_button.visible = false
	ai_button.visible = false
