extends Node2D

onready var player_1 = get_node("Player1")
onready var player_2 = get_node("Player2")
onready var current_player_label = get_node("current_player_label")

onready var players = [player_1, player_2]

var current_player
var tiles = {}

var lines = []

func _ready():
	_get_lines()
	_set_up_players()
	current_player = player_1
	current_player_label.text = current_player.player_name


func _set_up_players():
	player_1.player_name = GameInfo.player_1_name
	if GameInfo.opponent_tyoe == "human":
		player_2.player_name = GameInfo.player_2_name
	else:
		var save_player = player_2.duplicate()
		player_2.set_script(load(GameInfo.bot_script))
		player_2.mark_image = save_player.mark_image
		player_2.mark = save_player.mark
		player_2._ready()

func _check_board_for_win():
	for line in lines:
		if _are_tiles_same(line):
			print(current_player.player_name + " won")


func _are_tiles_same(tiles):
	return ((tiles[0].mark and tiles[1].mark and tiles[2].mark)
		and (tiles[0].mark == tiles[1].mark and tiles[0].mark == tiles[2].mark))


func _get_lines():
	lines.append([tiles[Vector2(-1,-1)], tiles[Vector2(0,-1)], tiles[Vector2(1,-1)]])
	lines.append([tiles[Vector2(-1, 0)], tiles[Vector2(0,0)], tiles[Vector2(1,0)]])
	lines.append([tiles[Vector2(-1,1)], tiles[Vector2(0,1)], tiles[Vector2(1,1)]])
	
	lines.append([tiles[Vector2(-1,-1)], tiles[Vector2(-1,0)], tiles[Vector2(-1,1)]])
	lines.append([tiles[Vector2(0,-1)], tiles[Vector2(0,0)], tiles[Vector2(0,1)]])
	lines.append([tiles[Vector2(1,-1)], tiles[Vector2(1,0)], tiles[Vector2(1,1)]])
	
	lines.append([tiles[Vector2(-1,-1)], tiles[Vector2(0,0)], tiles[Vector2(1,1)]])
	lines.append([tiles[Vector2(-1,1)], tiles[Vector2(0,0)], tiles[Vector2(1,-1)]])


func _get_tile_upper(tile):
	var tile_code = tile.map_pos
	tile_code.y -= 1
	if tile_code in tiles.keys():
		return tiles[tile_code]


func _get_tile_lower(tile):
	var tile_code = tile.map_pos
	tile_code.y += 1
	if tile_code in tiles.keys():
		return tiles[tile_code]


func _get_tile_right(tile):
	var tile_code = tile.map_pos
	tile_code.x += 1
	if tile_code in tiles.keys():
		return tiles[tile_code]


func _get_tile_left(tile):
	var tile_code = tile.map_pos
	tile_code.x -= 1
	if tile_code in tiles.keys():
		return tiles[tile_code]


func _on_tile_clicked():
	_check_board_for_win()
	_get_next_player()


func _get_next_player():
	var player_index = players.find(current_player)
	player_index += 1
	
	if player_index > players.size() - 1:
		player_index = 0
	
	current_player = players[player_index]
	current_player_label.text = current_player.player_name
	current_player.play()


func add_tile(tile):
	tiles[tile.map_pos] = tile
