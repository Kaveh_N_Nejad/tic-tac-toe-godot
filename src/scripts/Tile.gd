extends Node2D

signal clicked()

export(Vector2) var map_pos

onready var board = get_parent()
onready var sprite = get_node("Sprite")
onready var button = get_node("Button")

var mark

func _ready():
	board.add_tile(self)
	connect("clicked", board, "_on_tile_clicked")


func _on_Button_pressed():
	if board.current_player.player_type == "human":
		mark()


func mark():
	var player = board.current_player
	mark = player.mark
	sprite.texture = player.mark_image
	sprite.visible = true
	button.disabled = true
	button.visible = false
	emit_signal("clicked")
