extends "res://src/scripts/player.gd"

func _ready():
	player_type = "bot"
	player_name = "dummy"

func play():
	yield(board.get_tree().create_timer(1), "timeout")
	for tile in board.tiles.values():
		if not tile.mark:
			tile.mark()
			return
